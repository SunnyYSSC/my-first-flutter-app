import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
        child: new Container(
            padding: EdgeInsets.all(20.0),
            alignment: Alignment.center,
            color: Colors.deepPurple,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Spice Jet",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 35.0,
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.italic,
                                color: Colors.white))),
                    Expanded(
                        child: Text("From NYC to London",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 35.0,
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.italic,
                                color: Colors.white)))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Air New Zealand",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 35.0,
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.italic,
                                color: Colors.white))),
                    Expanded(
                        child: Text("From Auckland to Sydney",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 35.0,
                                fontFamily: 'Exo2',
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.italic,
                                color: Colors.white)))
                  ],
                ),
                new FlightImageAsset(),
                new FlightBookingButton()
              ],
            )));
  }
}

class FlightImageAsset extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/mystery.png');
    Image image = Image(image: assetImage, width: 250.0, height: 250.0);
    return Container(child: image);
  }

}

class FlightBookingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(top: 30.0),

      child: RaisedButton(
          color: Colors.deepOrange,
          child: Text(
            "Book your flight",
            style: TextStyle(
              fontSize: 15.0,
              fontFamily:"Exo2"
            ),),
          elevation: 6.0,
          onPressed: () {
            bookOnClick(context);
          }),
    );
  }

  void bookOnClick(BuildContext context){
    AlertDialog alertDialog = new AlertDialog(
      title: Text("Flight booked"),
      content: Text("Have a nice trip"),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) => alertDialog
    );
  }

}
